import { Injectable, Logger } from '@nestjs/common';
import { ThirdpartiesService } from './thirdparties/thirdparties.service';
import { Cron } from '@nestjs/schedule';
import { ArticleService } from './article/article.service';

/* Defining an interface. */
export interface checkStatus {
  ok: boolean;
  status: string;
}

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);

  /**
   * The constructor function is a special function that is called when a new instance of the class is
   * created
   * @param {ThirdpartiesService} thirdpartiesService - This is the service that we created earlier.
   * @param {ArticleService} articleService - This is the service that we created earlier.
   */
  constructor(
    private readonly thirdpartiesService: ThirdpartiesService,
    private readonly articleService: ArticleService,
  ) {}

  /**
   * It returns an object with two properties, ok and status
   * @returns An object with two properties, ok and status.
   */
  checkStatus(): checkStatus {
    return {
      ok: true,
      status: 'alive',
    };
  }

  /**
   * It drops all the documents in the article collection
   */
  async dropCollectionsData(): Promise<void> {
    await this.articleService.dropCollectionArticle();
    this.logger.log('drop documents article collection');
  }

  /* A cron job that runs every 1 hour. */
  @Cron('* * 1 * * *')
  async initialData(): Promise<void> {
    const articlesCountCollection =
      await this.articleService.findCountArticles();
    console.log(articlesCountCollection);
    const articles = await this.thirdpartiesService.getExternalArticles();
    // this.logger.log(articles);
    if (articlesCountCollection === 0) {
      await this.articleService.createMultipleArticles(articles);
    }
    await this.articleService.updateMultipleArticles(articles);
  }
}
