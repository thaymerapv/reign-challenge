import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom } from 'rxjs';
import { IExternalArticle } from './interfaces/externalArticle.interface';

@Injectable()
export class ThirdpartiesService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async getExternalArticles() {
    const externalArticleUrl = this.configService.get<string>(
      'EXTERNAL_ARTICLE_URL',
    );
    const {
      data: { hits },
    } = await lastValueFrom(this.httpService.get(externalArticleUrl));

    const articles: IExternalArticle[] = hits.filter(
      (arc: IExternalArticle) =>
        !(arc.story_title === null && arc.title === null),
    );

    return articles.map((article) => ({
      title: article.title || article.story_title,
      author: article.author || 'none',
      url: article.url || article.story_url || '/',
      externalTimestamp: article.created_at,
      externalID: article.objectID,
    }));
  }
}
