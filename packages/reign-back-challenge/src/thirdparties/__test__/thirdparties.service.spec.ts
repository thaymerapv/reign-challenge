import { Test, TestingModule } from '@nestjs/testing';
import { ThirdpartiesService } from '../thirdparties.service';
import { ConfigModule } from '@nestjs/config';
import { HttpModule, HttpService } from '@nestjs/axios';
import { lastValueFrom, of, Observable } from 'rxjs';
import { json } from 'stream/consumers';

const mockExternalArticle = {
  data: {
    hits: [
      {
        created_at: '2022-09-29T02:20:43.000Z',
        title: null,
        url: null,
        author: 'I_AM_A_SMURF',
        points: null,
        story_text: null,
        comment_text:
          'I mean... the Firefox build has NodeJS in it now, so in a sense Chromium is in the toolchain :-)',
        num_comments: null,
        story_id: 33008519,
        story_title:
          'Ken Thompson really did launch his "trusting trust" trojan attack in real life',
        story_url:
          'https://niconiconi.neocities.org/posts/ken-thompson-really-did-launch-his-trusting-trust-trojan-attack-in-real-life/',
        parent_id: 33010470,
        created_at_i: 1664418043,
        _tags: ['comment', 'author_I_AM_A_SMURF', 'story_33008519'],
        objectID: '33015633',
        _highlightResult: {
          author: {
            value: 'I_AM_A_SMURF',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'I mean... the Firefox build has <em>NodeJS</em> in it now, so in a sense Chromium is in the toolchain :-)',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Ken Thompson really did launch his "trusting trust" trojan attack in real life',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://niconiconi.neocities.org/posts/ken-thompson-really-did-launch-his-trusting-trust-trojan-attack-in-real-life/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
    ],
  },
};

describe('ThirdpartiesService', () => {
  let service: ThirdpartiesService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [
        ThirdpartiesService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(() =>
              of({
                ...mockExternalArticle,
              }),
            ),
          },
        },
      ],
    }).compile();

    service = module.get<ThirdpartiesService>(ThirdpartiesService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(httpService).toBeDefined();
  });

  it('should get an external item', async () => {
    // TODO: error with httpService response.
    jest.spyOn(httpService, 'get').mockReturnValue({
      of: jest.fn().mockReturnValue(mockExternalArticle),
    } as any);
    const data = await service.getExternalArticles();
    expect(data.length).toBe(1);
  });
});
