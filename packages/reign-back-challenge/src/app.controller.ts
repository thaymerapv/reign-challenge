import { Controller, Get } from '@nestjs/common';
import { AppService, checkStatus } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getApiStatus(): checkStatus {
    return this.appService.checkStatus();
  }
}
