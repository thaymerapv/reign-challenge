import { ApiProperty } from '@nestjs/swagger';
import { Expose, Transform } from 'class-transformer';

export class ArticleDto {
  @ApiProperty()
  @Transform(({ obj }) => {
    if (obj._id && typeof obj._id.toString === 'function') {
      return obj._id.toString();
    } else {
      return obj;
    }
  })
  @Expose()
  public readonly _id: string;

  @ApiProperty()
  @Expose()
  public readonly title: string;

  @ApiProperty()
  @Expose()
  public readonly author: string;

  @ApiProperty()
  @Expose()
  public readonly url: string;

  @ApiProperty()
  @Expose()
  public readonly externalTimestamp: string;

  @ApiProperty()
  @Expose()
  public readonly createdAt: string;

  @ApiProperty()
  @Expose()
  public readonly updatedAt: string;
}
