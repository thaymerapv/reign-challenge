import { PartialType } from '@nestjs/mapped-types';
import { CreateArticleDto } from './create-article.dto';

/* The UpdateArticleDto class extends the CreateArticleDto class, and it's a partial class */
export class UpdateArticleDto extends PartialType(CreateArticleDto) {}
