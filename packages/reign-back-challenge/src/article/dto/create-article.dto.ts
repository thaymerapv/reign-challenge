import { IsDateString, IsNotEmpty, IsString, IsUrl } from 'class-validator';

/* This class is used to create a new article */
export class CreateArticleDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  author: string;

  @IsNotEmpty()
  @IsString()
  @IsUrl()
  url: string;

  @IsNotEmpty()
  @IsString()
  externalID: string;

  @IsNotEmpty()
  @IsDateString()
  externalTimestamp: string;
}
