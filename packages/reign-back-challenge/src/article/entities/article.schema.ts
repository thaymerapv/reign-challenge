import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, LeanDocument } from 'mongoose';

export type ArticleDocument = Article & Document;
export type LeanArticleDocument = LeanDocument<ArticleDocument>;

/* It's a Mongoose schema that defines a document with a title, url, externalID, externalTimestamp, and
isDeleted property */
@Schema({ timestamps: true })
export class Article {
  @Prop({
    required: true,
    type: String,
    trim: true,
  })
  title: string;

  @Prop({
    type: String,
    trim: true,
  })
  author: string;

  @Prop({
    required: true,
    type: String,
    trim: true,
  })
  url: string;

  @Prop({
    required: true,
    type: String,
    trim: true,
    index: true,
  })
  externalID: string;

  @Prop({
    required: true,
    type: Date,
  })
  externalTimestamp: Date;

  @Prop({
    required: true,
    type: Boolean,
    default: false,
  })
  isDeleted: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
