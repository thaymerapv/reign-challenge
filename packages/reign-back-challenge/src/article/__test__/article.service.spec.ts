import { Test, TestingModule } from '@nestjs/testing';
import { Article, LeanArticleDocument } from '../entities/article.schema';
import { ArticleService } from '../article.service';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { CreateArticleDto } from '../dto/create-article.dto';

interface fakeBulkWriteResult {
  result: {
    ok: number;
    writeErrors: [];
    writeConcernErrors: [];
    insertedIds: [];
    nInserted: number;
    nUpserted: number;
    nMatched: number;
    nModified: number;
    nRemoved: number;
    upserted: [[Object]];
  };
}

describe('ArticleService', () => {
  let service: ArticleService;
  let model: Model<Article>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        {
          provide: getModelToken(Article.name),
          useValue: {
            find: jest.fn(),
            select: jest.fn(),
            insertMany: jest.fn(),
            bulkWrite: jest.fn(),
            sort: jest.fn(),
            lean: jest.fn(),
            exec: jest.fn(),
            findByIdAndUpdate: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    model = module.get<Model<Article>>(getModelToken(Article.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findAllArticles should return 3 articles', async () => {
    const articles = [
      {
        _id: '6335023517c9cd8094246241',
        title:
          'Ken Thompson really did launch his "trusting trust" trojan attack in real life',
        author: 'I_AM_A_SMURF',
        url: 'https://niconiconi.neocities.org/posts/ken-thompson-really-did-launch-his-trusting-trust-trojan-attack-in-real-life/',
        externalTimestamp: '2022-09-29T02:20:43.000Z',
        createdAt: '2022-09-29T02:25:57.686Z',
        updatedAt: '2022-09-29T05:21:08.832Z',
      },
      {
        _id: '63350b8917c9cd8094246512',
        title: 'When the push button was new, people were freaked',
        author: 'jdmdmdmdmd',
        url: 'https://daily.jstor.org/when-the-push-button-was-new-people-were-freaked/',
        externalTimestamp: '2022-09-28T21:46:08.000Z',
        createdAt: '2022-09-29T03:05:45.525Z',
        updatedAt: '2022-09-29T05:21:08.832Z',
      },
      {
        _id: '6334f3bcb2b78ad9fb3e323a',
        title: "Apple Has Reportedly Rejected TSMC's Chip Price Hike of 6%",
        author: 'Joeri',
        url: 'https://wccftech.com/apple-rejects-tsmc-chip-price-hike/',
        externalTimestamp: '2022-09-28T15:53:34.000Z',
        createdAt: '2022-09-29T01:24:12.611Z',
        updatedAt: '2022-09-29T05:21:08.832Z',
      },
    ];

    jest.spyOn(model, 'find').mockReturnValue({
      select: jest.fn().mockReturnValue({
        sort: jest.fn().mockReturnValue({
          lean: jest.fn().mockReturnValue({
            exec: jest.fn().mockReturnValue(articles) as any,
          }),
        }),
      }),
    } as any);

    const data = await service.findAll();
    expect(data.length).toBe(3);
  });

  it('should save multiple articles', async () => {
    const toCreate: CreateArticleDto[] = [
      {
        title: 'When the push button was new, people were freaked',
        author: 'jdmdmdmdmd',
        url: 'https://daily.jstor.org/when-the-push-button-was-new-people-were-freaked/',
        externalID: 'mockID1',
        externalTimestamp: '2022-09-28T21:46:08.000Z',
      },
      {
        title: "Apple Has Reportedly Rejected TSMC's Chip Price Hike of 6%",
        author: 'Joeri',
        url: 'https://wccftech.com/apple-rejects-tsmc-chip-price-hike/',
        externalID: 'mockID2',
        externalTimestamp: '2022-09-28T15:53:34.000Z',
      },
    ];
    const toReturned: LeanArticleDocument[] = [
      {
        _id: '63350b8917c9cd8094246512',
        ...toCreate.at(0),
        isDeleted: false,
        externalTimestamp: new Date('2022-09-28T21:46:08.000Z'),
      },
      {
        _id: '6334f3bcb2b78ad9fb3e323a',
        ...toCreate.at(1),
        isDeleted: false,
        externalTimestamp: new Date('2022-09-28T15:53:34.000Z'),
      },
    ];
    jest.spyOn(model, 'insertMany').mockResolvedValue(toReturned as any[]);
    const data = await service.createMultipleArticles(toCreate);
    expect(data.at(0)._id).toBe('63350b8917c9cd8094246512');
    expect(data.at(1)._id).toBe('6334f3bcb2b78ad9fb3e323a');
    expect(model.insertMany).toBeCalledWith(toCreate);
    expect(model.insertMany).toBeCalledTimes(1);
  });

  it('update or insert new articles', async () => {
    const toCreate: CreateArticleDto[] = [
      {
        title: 'When the push button was new, people were freaked',
        author: 'jdmdmdmdmd',
        url: 'https://daily.jstor.org/when-the-push-button-was-new-people-were-freaked/',
        externalID: 'mockID1',
        externalTimestamp: '2022-09-28T21:46:08.000Z',
      },
    ];
    const toReturned: fakeBulkWriteResult = {
      result: {
        ok: 1,
        writeErrors: [],
        writeConcernErrors: [],
        insertedIds: [],
        nInserted: 0,
        nUpserted: 1,
        nMatched: 0,
        nModified: 0,
        nRemoved: 0,
        upserted: [[Object]],
      },
    };
    jest.spyOn(model, 'bulkWrite').mockResolvedValue(toReturned as any);
    const data = await service.updateMultipleArticles(toCreate);
    expect(data.result.nUpserted).toBe(1);
    expect(model.bulkWrite).toBeCalledTimes(1);
  });

  it('remove article', async () => {
    const article: LeanArticleDocument = {
      _id: '6334f3bcb2b78ad9fb3e323a',
      title: 'When the push button was new, people were freaked',
      author: 'jdmdmdmdmd',
      url: 'https://daily.jstor.org/when-the-push-button-was-new-people-were-freaked/',
      externalID: 'mockID1',
      externalTimestamp: new Date('2022-09-28T21:46:08.000Z'),
      isDeleted: false,
    };
    const toReturned = {
      ...article,
      isDeleted: true,
    };
    jest.spyOn(model, 'findByIdAndUpdate').mockReturnValue({
      lean: jest.fn().mockReturnValue({
        exec: jest.fn().mockReturnValue({ ...toReturned }),
      }),
    } as any);

    const data = await service.remove(article._id);
    expect(data._id).toBe(article._id);
    expect(model.findByIdAndUpdate).toBeCalledTimes(1);
  });
});
