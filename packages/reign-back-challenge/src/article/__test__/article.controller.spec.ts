import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { of, lastValueFrom } from 'rxjs';
import { ArticleController } from '../article.controller';
import { ArticleService } from '../article.service';
import { Article, LeanArticleDocument } from '../entities/article.schema';

const fakeArticles: LeanArticleDocument[] = [
  {
    _id: '63350b8917c9cd8094246512',
    title: 'When the push button was new, people were freaked',
    author: 'jdmdmdmdmd',
    url: 'https://daily.jstor.org/when-the-push-button-was-new-people-were-freaked/',
    externalID: 'mockID1',
    externalTimestamp: new Date('2022-09-28T21:46:08.000Z'),
    isDeleted: false,
  },
  {
    _id: '6334f3bcb2b78ad9fb3e323a',
    title: "Apple Has Reportedly Rejected TSMC's Chip Price Hike of 6%",
    author: 'Joeri',
    url: 'https://wccftech.com/apple-rejects-tsmc-chip-price-hike/',
    externalID: 'mockID2',
    externalTimestamp: new Date('2022-09-28T15:53:34.000Z'),
    isDeleted: false,
  },
];

describe('ArticleController', () => {
  let controller: ArticleController;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [
        {
          provide:ArticleService,
          useValue:{
            findAll: jest.fn().mockReturnValue(fakeArticles),
            remove: jest.fn().mockReturnValue(fakeArticles.at(1))
          }
        },
      ],
    }).compile();

    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should get an array of artices', async () => {
    const articles = await controller.findAll();
    expect(articles.length).toBe(2);
  });

  it('should remove one article by id', async () => {
    const article = await controller.remove(fakeArticles.at(0)._id);
    expect(article).toBe(fakeArticles.at(1));
  });
});
