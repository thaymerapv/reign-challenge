import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import {
  Article,
  ArticleDocument,
  LeanArticleDocument,
} from './entities/article.schema';

@Injectable()
export class ArticleService {
  private readonly logger = new Logger(ArticleService.name);

  findCountArticles() {
    return this.articleModel.count();
  }

  /**
   * The constructor function is used to inject the Article model into the ArticleService class
   * @param articleModel - This is the model that we will use to perform CRUD operations.
   */
  constructor(
    @InjectModel(Article.name)
    private readonly articleModel: Model<ArticleDocument>,
  ) {}

  /**
   * It takes an array of CreateArticleDto objects and inserts them into the database
   * @param {CreateArticleDto[]} createArticleDto - CreateArticleDto[]
   */
  async createMultipleArticles(
    createArticleDto: CreateArticleDto[],
  ): Promise<ArticleDocument[]> {
    try {
      const articles = await this.articleModel.insertMany(createArticleDto);
      return articles;
    } catch (error) {
      this.logger.error(error);
      this.logger.error(
        'an error occurred inserting the documents of the articles',
      );
      throw new InternalServerErrorException();
    }
  }

  /**
   * It returns a promise that resolves to an array of LeanArticleDocument objects
   * @returns An array of LeanArticleDocument objects.
   */
  findAll(): Promise<LeanArticleDocument[]> {
    return this.articleModel
      .find({ isDeleted: false })
      .select('_id title author url externalTimestamp createdAt updatedAt')
      .sort({ externalTimestamp: -1 })
      .lean()
      .exec();
  }

  /**
   * It takes an array of UpdateArticleDto objects, and for each one, it creates a MongoDB bulkWrite
   * operation that will update the document with the same externalID as the DTO, or insert a new
   * document if no document with that externalID exists
   * @param {UpdateArticleDto[]} updateArticleDto - UpdateArticleDto[]
   */
  async updateMultipleArticles(
    updateArticleDto: UpdateArticleDto[],
  ): Promise<unknown | any> {
    try {
      const articles = [];
      updateArticleDto.forEach(({ ...dto }) => {
        articles.push({
          updateOne: {
            filter: { externalID: dto.externalID },
            update: { ...dto },
            upsert: true,
          },
        });
      });
      const bulkWriteArticles = await this.articleModel.bulkWrite(articles);
      return bulkWriteArticles;
    } catch (err) {
      console.log(err);
      this.logger.error(
        'an error occurred inserting the documents of the articles',
      );
      throw new InternalServerErrorException();
    }
  }

  /**
   * It finds an article by its id and deletes it
   * @param {string} id - The id of the article to be deleted.
   * @returns A promise that resolves to a LeanArticleDocument
   */
  remove(id: string): Promise<LeanArticleDocument> {
    return this.articleModel
      .findByIdAndUpdate({ _id: id }, { isDeleted: true })
      .lean()
      .exec();
  }

  /**
   * It removes all documents from the article collection
   */
  async dropCollectionArticle(): Promise<void> {
    await this.articleModel.deleteMany({});
  }
}
