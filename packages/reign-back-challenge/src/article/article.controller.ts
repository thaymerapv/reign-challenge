import { Controller, Get, Param, Delete } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleDto } from './dto/article.dto';
import { ParseMongoIdPipe } from '../common/pipes/parse-mongo-id.pipe';
import { LeanArticleDocument } from './entities/article.schema';
import { Serialize } from '../common/interceptors/serialize.interceptor';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorResponseDto } from '../common/dtos/error-response.dto';

@ApiTags('Article Endpoints')
@Controller('article')
@Serialize(ArticleDto)
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  @ApiOkResponse({ type: () => ArticleDto, isArray: true })
  @ApiOperation({ description: 'get all articles :)' })
  @ApiBadRequestResponse({ type: () => ErrorResponseDto })
  @ApiInternalServerErrorResponse({ type: () => ErrorResponseDto })
  findAll(): Promise<LeanArticleDocument[]> {
    return this.articleService.findAll();
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => ArticleDto })
  @ApiOperation({ description: 'remove an article' })
  @ApiResponse({ type: () => ArticleDto })
  @ApiBadRequestResponse({ type: () => ErrorResponseDto })
  @ApiInternalServerErrorResponse({ type: () => ErrorResponseDto })
  remove(@Param('id', ParseMongoIdPipe) id: string) {
    return this.articleService.remove(id);
  }
}
