import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ThirdpartiesService } from './thirdparties/thirdparties.service';
import { ArticleService } from './article/article.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: ThirdpartiesService,
          useValue: {},
        },
        {
          provide: ArticleService,
          useValue: {},
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Object Api Status', () => {
      const data = appController.getApiStatus();
      expect(data.ok).toBe(true);
      expect(data.status).toBe('alive');
    });
  });
});
