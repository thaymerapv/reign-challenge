## Description

Api created in nest for reign technical test


## Environment file
```bash
$ touch .env

# add properties

NODE_ENV=
PORT=
DB_PASSWORD=
DB_NAME=
DB_HOST=
DB_PORT=
DB_USERNAME=
EXTERNAL_ARTICLE_URL=

```
## Installation

```bash
$ npm install
```

## Database
It is required that you have MongoDB running

## Running the app
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

thaymerapv@gmail.com

