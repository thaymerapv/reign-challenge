import { Component, OnInit } from '@angular/core';
import { ArticlesService } from 'src/app/services/articles/articles.service';
import { IArticle } from 'src/app/services/articles/interfaces/article.interface';
import { lastValueFrom, timer } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles:IArticle[] = [];

  constructor(private readonly articleService: ArticlesService) { }

  ngOnInit() {
    timer(0, 600000).subscribe(() => {
      this.getArticles();
    });
  }

  private async getArticles(){
    this.articles = await lastValueFrom(this.articleService.getArticles())
    // change this answer of observable a promesa up
    // this.articleService.getArticles().subscribe((res:IArticle[])=>{
    //     this.articles = res;
    // })
  }

   removeArticle(id:string){
    this.articleService.removeArticle(id).subscribe((res:IArticle)=>{
      alert(`Article [${res.title}] with id ${id} successful remove`)
      this.getArticles()
    })
  }

}

