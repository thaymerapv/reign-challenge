import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Article } from './models/Article.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent {
  @Input() article: Article | null= null;
  @Output() removeArticle:EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  async obtainedIdArticleForRemove(id:string){
    this.removeArticle.next(id);
  }
}
