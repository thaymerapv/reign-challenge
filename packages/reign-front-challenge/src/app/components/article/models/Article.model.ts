export class Article {
   _id: string = '';

   title: string = '';

   author: string = '';

   url: string = '';

   externalTimestamp: string = '';

   createdAt: string = '';

   updatedAt: string = '';
}
