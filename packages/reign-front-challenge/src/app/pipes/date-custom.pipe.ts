import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateCustom'
})
export class DateCustomPipe implements PipeTransform {

  transform(date: Date | string): unknown {
    const now = new Date();
    date = new Date(date);
    console.log('date', date)

    if(now.getDay() === date.getDay()){
      return new DatePipe('en-US').transform(date, 'shortTime');
    }
    if((now.getDay() - 1) === date.getDay()){
      return 'yesterday'
    }

    return new DatePipe('en-US').transform(date, 'MMM d');;
  }

}
