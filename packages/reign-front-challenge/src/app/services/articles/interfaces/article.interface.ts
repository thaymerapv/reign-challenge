export interface IArticle {
  readonly _id: string;

  readonly title: string;

  readonly author: string;

  readonly url: string;

  readonly externalTimestamp: string;

  readonly createdAt: string;

  readonly updatedAt: string;
 }
