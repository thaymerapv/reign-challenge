import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Article } from './models/article.model';
import { IArticle } from './interfaces/article.interface';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private apiUrl: string = environment.apiUrl;
  constructor(private readonly httpClient: HttpClient) { }

  getArticles(): Observable<IArticle[]>{
    return this.httpClient.get<IArticle[]>(`${this.apiUrl}/article`)
  }

  removeArticle(id:string): Observable<IArticle>{
    return this.httpClient.delete<IArticle>(`${this.apiUrl}/article/${id}`)
  }
}
