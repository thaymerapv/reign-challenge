import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ArticlesService } from './services/articles/articles.service';
import { ArticleComponent } from './components/article/article.component';
import { DateCustomPipe } from './pipes/date-custom.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArticleComponent,
    DateCustomPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule
  ],
  providers: [ArticlesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
