
## indicators
* inside each folder is your README.md file in case of any query
* the reign-back-challenge folder contains all the corresponding logic of the api, it has its documentation and testing
  * run tests in api
    ```bash
      cd packages/reign-back-challenge
      # run command
      npm run test
    ```
* the reign-front-challenge folder did two things that weren't covered in the wireframe, every 10 minutes it makes a request to the api to get the new articles added and a placed alert is generated 
  for when an article is deleted.
* some articles don't return the url so point it to the root path "/"
* To see the change of the dates of the reflected articles, you must change the day of the article in the variable "externalTimestamp" with that the pipe in angular will place the corresponding formats
* do not complete extra requirements

## run the project
* create the .env file
* copy everything that is in the .env.example file to the .env file
* execute the command
    ```bash
        docker compose up

        # they can also run in the background with
        docker compose up -d
    ```

# endpoints
* API -> http://localhost:3000/docs
* CLIENT -> http://localhost:4200/
* MONGODB URI -> mongodb://localhost:27017

